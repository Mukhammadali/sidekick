import * as Store from 'electron-store';
import * as electron from 'electron';
import * as path from 'path';
import { exec } from 'child_process';
import * as process from 'process'

// The following statements needs to be before placed before importing sidekick-terminal-server
process.env.SIDEKICK_NODE_ENV = electron.app.isPackaged ? 'prod' : 'dev';
const PORT = (process.env.SIDEKICK_NODE_ENV === 'dev' && process.env.PORT) ? parseInt(process.env.PORT) : 3000;
// process.env.SIDEKICK_NODE_ENV = 'prod';


import { SessionsManager, TerminalEntry } from '@foundryapp/sidekick-terminal-server';

import { BrowserWindow, app, ipcMain, Notification } from 'electron';
import { inspect, promisify } from 'util';
import isdev from './isdev';

import {
  getDatabaseUser,
  getUserDefaultTeam,
  getTeamMembers,
} from './firebase';

const ax = require('@foundryapp/accessibility-node');

function logInRendered(...args: any) {
  win?.webContents.send('console', args);
}

const oldLog = console.log;
console.log = (...args: any) => {
  oldLog(...args);
  logInRendered('log', ...args);
}

const oldError = console.error;
console.error = (...args: any) => {
  oldError(...args);
  logInRendered('error', ...args);
}

const icon = electron.nativeImage.createFromPath(app.getAppPath() + '/public/icon.png');
app.dock.setIcon(icon);

const manager = new SessionsManager();

let win: BrowserWindow | undefined = undefined;
let isNextShellSessionForSharing = false;
let sharingSessionID: string | undefined = undefined;
let sharedShellSession: TerminalEntry | undefined = undefined;

const store = new Store();
let activeTeamID: string | undefined = undefined;
let isAppReady = false;

interface WindowNotification {
  name: string;
  payload: any;
}
const windowNotifQueue: WindowNotification[] = [];

const pendingSessionInvites: any[] = [];

function executeCommand(command: string): Promise<string> {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout) => {
      if (error) {
        return reject(error);
      }
      return resolve(stdout);
    });
  });
}

function createWindowAndFocus() {
  const [winWidth, winHeight] = store.get('winSize', [280, 600]);
  const [winPosX, winPosY] = store.get('winPosition', [undefined, undefined]);

  win = new BrowserWindow({
    x: winPosX,
    y: winPosY,
    width: winWidth,
    height: winHeight,
    minWidth: 280,
    minHeight: 50,
    titleBarStyle: 'hidden',
    backgroundColor: '#1D1D1D',
    fullscreenable: false,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      // devTools: isdev,
      // nodeIntegrationInSubFrames: true,
      // webSecurity: false,
      // allowRunningInsecureContent: true,
    },
  });

  win.on('restore', () => {
    console.log('Window restored');
    if (!win) { return; }
  });

  win.on('resize', () => {
    if (win) {
      const [width, height] = win.getSize();
      store.set('winSize', [width, height]);
    }
  });

  win.on('moved', () => {
    if (win) {
      const [x, y] = win.getPosition();
      store.set('winPosition', [x, y]);
    }
  });

  win.on('close', () => {
    if (win) {
      const [width, height] = win.getSize();
      store.set('winSize', [width, height]);
      const [x, y] = win.getPosition();
      store.set('winPosition', [x, y]);
    }
  })

  win.on('closed', () => {
    win = undefined;
  });

  win.webContents.on('crashed', (event, killed) => {
    console.error('web window crashed', killed, inspect(event, { depth: null }));
  });

  if (isdev) {
    win.loadURL(`http://localhost:${PORT}/index.html`);
    // Hot Reloading
    require('electron-reload')(__dirname, {
      electron: path.join(__dirname, '..', '..', 'node_modules', '.bin', 'electron'),
      forceHardReset: true,
      hardResetMethod: 'exit'
    });

    win.webContents.openDevTools();
  } else {
    win.loadURL(`file://${__dirname}/../index.html#/index.html`);
  }

  win.focus();
}

app.once('ready', async () => {
  isAppReady = true;
  createWindowAndFocus();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (isAppReady && !win) {
    createWindowAndFocus();
  }
});

manager.connectionEmitter.on('connect_error', ({ error }) => {
  // TODO
  win?.webContents.send('connect_error', { error });
});

manager.connectionEmitter.on('connect_timeout', ({ timeout }) => {
  // TODO
  win?.webContents.send('connect_timeout', { timeout });
});

manager.connectionEmitter.on('disconnect', () => {
  // TODO
  win?.webContents.send('disconnect');
});

manager.sessionsEmitter.on('hangout', ({ hostId, sessionId }: { hostId: string, sessionId: string }) => {
  console.log('session hangout (hostId, sessionId):', hostId, sessionId);
  win?.webContents.send('session-hangout', { hostID: hostId, sessionID: sessionId });
});

manager.terminals.on('insert', async (terminal: TerminalEntry) => {
  console.log('Local shell session opened [main]:', terminal);
  console.log('isNextShellSessionForSharing', isNextShellSessionForSharing);
  if (isNextShellSessionForSharing) {
    if (sharingSessionID) {
      win?.webContents.send('host-shell-session-for-sharing-created');
      manager.acceptPeerSession(sharingSessionID, terminal.terminalId);
    } else {
      // TODO: Notify renderer?
      console.error('The just registered shell session is meant for sharing but session ID is undefined');
    }
    isNextShellSessionForSharing = false;
  } else {
    win?.webContents.send('local-shell-session-opened', terminal);
  }
});

manager.terminals.on('delete', (terminal: TerminalEntry) => {
  console.log('Local shell session closed [main]:', terminal);
  win?.webContents.send('local-shell-session-closed', terminal);
});

manager.usersEmitter.on('online_users', async (uids) => {
  console.log('online users (uids, activeTeamID, !!win)', uids, activeTeamID, !!win);
  if (!win || !activeTeamID) { return; }
  const members = (await getTeamMembers(activeTeamID)).map(member => ({ ...member, isOnline: uids.includes(member.userID) }));
  win.webContents.send('update-presence', { members });
});

manager.sessionsEmitter.on('invite', async (hostID: string, sessionID: string, hostShellSession: TerminalEntry) => {
  console.log('invite from host [main] (hostID, sessionID, hostShellSession):', hostID, sessionID, hostShellSession);
  const databaseUser = await getDatabaseUser(hostID) as any;

  const winNotif = {
    name: 'session-invite',
    payload: { user: databaseUser, sessionID, hostShellSession },
  }
  if (win) {
    win.webContents.send(winNotif.name, winNotif.payload);
  } else {
    console.log('[main] window not available, saving session invite for future', winNotif);
    pendingSessionInvites.push(winNotif.payload);
  }
  const notif = new Notification({
    title: 'Terminal Session Invite',
    body: `${databaseUser.username} wants to share terminal with you.`,
    /*
    // In order for extra notification buttons to work on macOS your app must meet the following criteria.
    // https://www.electronjs.org/docs/api/structures/notification-action#button-support-on-macos
    actions: [
      {
        type: 'button',
        text: 'Accept',
      },
      {
        type: 'button',
        text: 'Reject',
      }
    ],
    */
  });
  /*
  notif.on('action', (event, index) => {
    console.log('event', event);
    console.log('index', index);
  });
  */
  notif.on('click', (event) => {
    if (!win) {
      createWindowAndFocus();
    } else {
      if (win.isMinimized()) {
        win.restore();
      } else {
        win.focus();
      }
    }
  });
  notif.show();
  /*
  const notif = new Notification('Terminal Session Invite', {
    body: `You teammate ${databaseUser.username} wants to share a terminal session with you.`,
    actions: [
      {
        action: 'button',
        title: 'Accept',
      },
      {
        action: 'button',
        title: 'Reject',
      }
    ],
  });
  */

  // Either cancel or accept session request
  // managere.rejectPeerSession()
});

manager.sessionsEmitter.on('rejected', async (clientID: string, sessionID: string) => {
  console.log('rejected from client [main] (clientID, sessionID)', clientID, sessionID);
  // Host invited a client and client rejected the session. Host is now notified.
  const databaseUser = await getDatabaseUser(clientID) as any;
  const notif = new Notification({
    title: 'Invite rejected',
    body: `${databaseUser.username} rejected your session invite`,
  });
  notif.show();
});

manager.sessionsEmitter.on('accepted', async (clientID: string, sessionID: string) => {
  console.log('accepted from client [main] (clientID, sessionID)', clientID, sessionID);
  // Host invited a client and client accepted the session. Host is now notified.
  const databaseUser = await getDatabaseUser(clientID) as any;
  const notif = new Notification({
    title: 'Invite accepted',
    body: `${databaseUser.username} accepted your session invite`,
  });
  notif.show();
  // win?.webContents.send('session-invite-accepted', clientID, sessionID);
});


ipcMain.handle('is-process-trusted', () => {
  const isTrusted = ax.isProcessTrusted();
  return isTrusted;
});

ipcMain.handle('is-stcl-set-up', async () => {
  console.log('checking stcl installation');
  try {
    console.log('checking default shell...');
    const defaultShell = process.env.SHELL || await executeCommand('echo $SHELL');
    console.log('default shell', defaultShell);
    try {
      const stcl = await executeCommand('which /usr/local/bin/stcl');
      console.log('stcl location', stcl);
    } catch (error) {
      throw new Error('stcl is not installed');
    }

    if (defaultShell.includes('bash')) {
      return store.get('shell_init_bash', false);
    } else if (defaultShell.includes('zsh')) {
      return store.get('shell_init_zsh', false);
    } else if (defaultShell.includes('fish')) {
      return store.get('shell_init_fish', false);
    } else {
      throw new Error(`Cannot determine user\'s shell ${defaultShell}`);
    }

  } catch (error) {
    console.log('error cheking stcl installation');
    console.error(error);
    return false;
  }
});

ipcMain.handle('change-team', async (event, { teamName }: { teamName: string }) => {
  if (teamName && manager.connected) {
    await manager.changeTeam(teamName);
  }
});

ipcMain.handle('do-stcl-set-up', async () => {
  console.log('installing stcl');
  try {
    await executeCommand('curl https://storage.googleapis.com/get-stcl/install -sSfL | sh');
    console.log('downloaded stcl');
    const defaultShell = process.env.SHELL || await executeCommand('echo $SHELL');
    await executeCommand('which /usr/local/bin/stcl');
    const stclInstallPath = '/usr/local/bin/stcl';

    const sidekickCommentStart = `##########\tRequired by Sidekick.app\t\t\t##########\n##########\tMust at the top of the file!\t##########`;
    const sidekickCommentEnd = `##########\tRequired by Sidekick.app\t\t\t##########`;

    if (defaultShell.includes('bash')) {
      const isInitSet = store.get('shell_init_bash', false);
      if (isInitSet) {
        console.log('stcl initializer already added to the .zshenv');
        return true;
      }

      const envCommand = `${stclInstallPath}\n((SIDEKICK_FORK_COUNT++))`;
      const shellConfigPath = '~/.bash_profile';
      await executeCommand(`echo "${sidekickCommentStart}\n${envCommand}\n${sidekickCommentEnd}\n\n$(cat ${shellConfigPath})" > ${shellConfigPath}`);

      store.set('shell_init_bash', true);
      console.log('bash installation completed');
    } else if (defaultShell.includes('zsh')) {
      const isInitSet = store.get('shell_init_zsh', false);
      if (isInitSet) {
        console.log('stcl initializer already added to the .zshenv');
        return true;
      }

      const envCommand = `${stclInstallPath}\n((SIDEKICK_FORK_COUNT++))`;
      const shellConfigPath = '~/.zshenv';
      await executeCommand(`echo "${sidekickCommentStart}\n${envCommand}\n${sidekickCommentEnd}\n\n$(cat ${shellConfigPath})" > ${shellConfigPath}`);

      store.set('shell_init_zsh', true);
      console.log('zsh installation completed');
    } else if (defaultShell.includes('fish')) {
      const isInitSet = store.get('shell_init_fish', false);
      if (isInitSet) {
        console.log('stcl initializer already added to the config.fish');
        return true;
      }

      const fishProfileFilePath = '~/.config/fish/config.fish';

      // Save content of a fish profile file to the FISH_PROFILE_CONTENT variable.
      const cmd_saveProfileFileContent = `FISH_PROFILE_CONTENT=$(cat ${fishProfileFilePath})`;
      // Append content of the FISH_PROFILE_CONTENT variable and rewrite with it the fish profile file.
      const cmd_appendProfileAndSaveBack = `printf "${sidekickCommentStart}\n${stclInstallPath}\nset SIDEKICK_FORK_COUNT (math \\$SIDEKICK_FORK_COUNT + 1)\n${sidekickCommentEnd}\n\n$FISH_PROFILE_CONTENT" > ${fishProfileFilePath}`;

      await executeCommand(`${cmd_saveProfileFileContent};${cmd_appendProfileAndSaveBack}`);
      store.set('shell_init_fish', true);
    } else {
      throw new Error(`Cannot determine user\'s shell ${defaultShell}`);
    }
    return true;
  } catch (error) {
    console.log(error);
    console.error(error);
    return false;
  }
});

ipcMain.handle('request-shell-sessions', () => {
  return manager.terminals.data;
});

ipcMain.handle('share-shell-session', async (event, { userID, shellSession }: { userID: string, shellSession: TerminalEntry }) => {
  console.log('share shell session [main] (userID, shellSession):', userID, shellSession);
  manager.createPeerSession(userID, shellSession.terminalId);
});

ipcMain.handle('request-pending-session-invite', () => {
  return pendingSessionInvites;
});

ipcMain.handle('disconnect', () => {
  manager.disconnect();
});

ipcMain.on('accept-shell-session', async (event, { sessionID, hostShellSession }: { sessionID: string, hostShellSession: TerminalEntry }) => {
  console.log('Will accept shell session [main] (sessionID, hostShellSession):', sessionID, hostShellSession);

  isNextShellSessionForSharing = true
  sharingSessionID = sessionID;

  try {
    if (ax.isAppRunning('com.apple.Terminal')) {
      const newTermWindow = promisify(ax.createNewWindow);
      console.log('Terminal is running, will create a new window');
      await newTermWindow('com.apple.Terminal');
    } else {
      console.log('Terminal is not running, will launch it');
      const launchTerminal = promisify(ax.launchApp);
      await launchTerminal('com.apple.Terminal');
    }
  } catch (error) {
    isNextShellSessionForSharing = false;
    sharingSessionID = undefined;
    console.error('Failed to create new terminal app window', error);
    // TODO: Notify renderer that we failed?
  }
});

ipcMain.on('reject-shell-session', (event, { sessionID }: { sessionID: string }) => {
  console.log('will reject shell session [main]:', sessionID);
  manager.rejectPeerSession(sessionID);
});

ipcMain.on('turn-terminal-highlight-on', (event, { terminalID, message, color }) => {
  console.log('turn highlight on', terminalID, message);
  manager.turnTerminalHighlightOn(terminalID, message, color);
});

ipcMain.on('turn-terminal-highlight-off', (event, { terminalID }) => {
  console.log('turn highlight off', terminalID);
  manager.turnTerminalHighlightOff(terminalID);
});

ipcMain.on('view-ready', async () => {
  /*
  console.log('view-ready', win);
  if (!win) { return; }
  const length = windowNotifQueue.length;
  for (let i = 0; i < length; i++) {
    const notif = windowNotifQueue.shift();
    console.log('[main] emptying window notif queue', notif?.name, notif?.payload);
    if (notif) {
      win.webContents.send(notif.name, notif.payload);
    }
  }
  */
});

ipcMain.on('user-authenticated', async (event, { userID, teamID }: { userID: string, teamID: string }) => {
  console.log('User authenticated [main]', userID, teamID);
  //const team = await getUserDefaultTeam(userID) as any;
  //activeTeamID = team.teamID;
  activeTeamID = teamID;
  console.log('<<connecting>>');
  await manager.connect(userID);
  await manager.changeTeam(teamID);
});
