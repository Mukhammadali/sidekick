import * as firebase from 'firebase/app';
import 'firebase/firestore';

export interface TeamMember {
  username: string;
  userID: string;
}

const config = {
  apiKey: "AIzaSyBBVqZNSKc17L_BcNwhofYJKsUkTZ1MgaI",
  authDomain: "terminal-sidekick.firebaseapp.com",
  databaseURL: "https://terminal-sidekick.firebaseio.com",
  projectId: "terminal-sidekick",
  storageBucket: "terminal-sidekick.appspot.com",
  messagingSenderId: "692698844415",
  appId: "1:692698844415:web:dbcbe0c35b9291e21b90ca",
  measurementId: "G-NXGHJRWEF3"
};
firebase.initializeApp(config);

const db = firebase.firestore();

export async function isUserInTeam(userID: string, teamID: string): Promise<boolean> {
  const teamDoc = await db.collection('teams').doc(teamID).get();
  const data = teamDoc.data();
  if (data) {
    const { members }: { members: string[] } = data as any;
    return members.includes(userID);
  } else {
    return false;
  }
}

export async function getDatabaseUser(userID: string) {
  const query = await db.collection('users').where('userID', '==', userID).get();
  if (query.empty) {
    throw new Error(`User ${userID} does not exist`);
  }
  return query.docs[0].data();
}

export async function getUserDefaultTeam(userID: string) {
  const query = await db.collection('teams').where('members', 'array-contains', userID).get();
  // Just take the first team for a user.
  // Each user must have team because default team is created on sign up anyway.
  return query.docs[0].data();
}

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

export async function getTeamMembers(teamID: string): Promise<TeamMember[]> {

  console.log('teamID', teamID);
  const team = await db.collection('teams').doc(teamID).get();
  const data = team.data() as { members: Array<string> };

  if (team.exists && data) {
    const members = await Promise.all(data.members.map(async (uid: string) => {
      try {
        const userData = await getDatabaseUser(uid);
        return { username: userData.username as string, userID: userData.userID as string };
      } catch (error) {
        console.error(error.message);
        return undefined;
      }
    }));
    return members.filter(notEmpty);
  }
  return [];

  // const members: TeamMember[] = [];
  // for (let uid of onlineUserIDs) {
  //   if (await isUserInTeam(uid, teamID)) {
  //     const userData = await getDatabaseUser(uid) as any;
  //     console.log('userData', userData);
  //     members.push({ username: userData.username, userID: userData.userID });
  //   }
  // }
}

