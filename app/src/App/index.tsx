import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import * as firebase from 'firebase/app';

import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import Loading from 'components/Loading';
import Home from 'Home';
import Init from 'Init';
import SignUp from 'SignUp';
import SignIn from 'SignIn';
import AccessibilitySetup from 'AccessibilitySetup';

import { isSTCLSetUp, isProcessTrusted } from '../mainProcess';


// The electron window is set to be frameless.
// Frameless window stops being draggable - this is the solution.
const DragHeader = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 30px;
  width: 100%;
  background: transparent;
  -webkit-app-region: drag;
  -webkit-user-select: none;
`;

const Content = styled.div`
  margin: 30px 10px 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  max-width: 100%;
`;

const AppDiv = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`;

const ActiveViewDiv = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

function App() {
  const [isLoading, setIsLoading] = useState(true);
  // const [isInstalled, setIsInstalled] = useState(false);
  const [isAccessibilityAllowed, setIsAccessibilityAllowed] = useState(false);
  const [isSetUp, setIsSetUp] = useState(false);

  useEffect(() => {
    async function check() {
      const [setUp, accessibility] = await Promise.all([isSTCLSetUp(), isProcessTrusted()])

      setIsSetUp(setUp);
      setIsAccessibilityAllowed(accessibility);

      setIsLoading(false);
    }

    check();
  }, []);

  return (
    <AppDiv>
      <DragHeader />
      <Content>
        {isLoading && <Loading text="" />}

        {!isLoading && (
          <Router>
            <Switch>
              <Route
                path="/index.html"
                exact
              >
                <Redirect to={{ pathname: "/" }} />
              </Route>

              <Route
                path="/"
                exact
              >
                <>
                  {!isAccessibilityAllowed && (
                    <Redirect to={{ pathname: "/accessibility" }} />
                  )}

                  {!isSetUp && isAccessibilityAllowed && (
                    <Redirect to={{ pathname: "/init" }} />
                  )}

                  {firebase.auth().currentUser && isAccessibilityAllowed && isSetUp && (
                    <Redirect to={{ pathname: "/home" }} />
                  )}

                  {!firebase.auth().currentUser && isAccessibilityAllowed && isSetUp && (
                    <>
                      <Redirect to={{ pathname: "/sign-in" }} />
                    </>
                  )}
                </>
              </Route>

              <Route
                path="/home"
                component={Home}
                exact
              />

              <Route
                path="/accessibility"
                render={() => <AccessibilitySetup setIsAccessibilityAllowed={setIsAccessibilityAllowed}></AccessibilitySetup>}
                exact
              />

              <Route
                path="/init"
                render={() => <Init setIsSetUp={setIsSetUp}></Init>}
                exact
              />

              <Route
                path="/sign-up"
                component={SignUp}
                exact
              />
              <Route
                path="/sign-in"
                component={SignIn}
                exact
              />
            </Switch>
          </Router>
        )}
      </Content>
    </AppDiv>
  );
}

export default App;
