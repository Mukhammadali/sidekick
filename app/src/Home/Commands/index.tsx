import React from 'react';
import styled from 'styled-components';
import firebase from 'firebase/app';

import { useCollectionData } from 'react-firebase-hooks/firestore';

import ErrorMessage from 'components/ErrorMessage';
import Command, { CommandInfo } from './Command';


const Content = styled.div`
  display: flex;
  flex-direction: column;
`;


interface CommandsProps {
  teamID: string;
  openCommandModal: (command: CommandInfo) => void;
}

function Commands(props: CommandsProps) {
  const [commands, loading, error] = useCollectionData<CommandInfo>(
    firebase.firestore().collection('teams').doc(props.teamID).collection('commands'),
    {
      idField: 'id',
    }
  );

  return (
    <Content>
      {error && !loading &&
        <ErrorMessage>
          {error.message}
        </ErrorMessage>
      }
      {!error && !loading && commands && commands.map((command =>
        <Command
          openCommandModal={props.openCommandModal}
          key={command.name}
          command={command}
        />
      ))}
    </Content>
  );
}


export default Commands;
