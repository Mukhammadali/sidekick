import React from 'react';
import styled from 'styled-components';
import copy from 'copy-to-clipboard';

import { ReactComponent as CopyIcon } from 'img/copy.svg';
import { ReactComponent as DotsIcon } from 'img/dots.svg';

export interface CommandInfo {
  name: string;
  value: string;
  id: string;
}

const Content = styled.div`
  border-radius: 5px;
  padding: 3px 5px 3px 10px;
  margin-bottom: 5px;
  /* border: 1px solid #5C4FED; */
  background: #5C4FED;
  display: flex;
  align-items: center;
`;

const Name = styled.div`
  color: #FBFBFB;
  max-width: 200px;

  white-space: nowrap;
  text-overflow: ellipsis;

  overflow-x: hidden;
  font-size: 14px;
  font-weight: 500;
`;

const ControlWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  flex: 1;
  align-items: center;
`;

const StyledDotsIcon = styled(DotsIcon)`
  * {
    fill: #FFF;
  }
`;

const StyledCopyIcon = styled(CopyIcon)`
  * {
    stroke: #FFF;
  }
`;

const Settings = styled.div`
  display: flex;
  align-items: center;
  padding: 5px 5px;

  :hover {
    cursor: pointer;
  }
`;

const Copy = styled.div`
  display: flex;
  align-items: center;
  padding: 5px 3px;
  margin-right: 3px;

  :hover {
    cursor: pointer;
  }
`;

interface CommandProps {
  command: CommandInfo;
  openCommandModal: (command: CommandInfo) => void;
}

function Command(props: CommandProps) {
  function openEditCommandModal() {
    props.openCommandModal(props.command);
  }

  return (
    <Content>
      <Name>
        {props.command.name}
      </Name>
      <ControlWrapper>
        <Copy onClick={() => { copy(props.command.value, { message: 'Command copied to clipboard' }) }}>
          <StyledCopyIcon />
        </Copy>
        <Settings
          onClick={openEditCommandModal}
        >
          <StyledDotsIcon />
        </Settings>
      </ControlWrapper>
    </Content>
  );
}


export default Command;
