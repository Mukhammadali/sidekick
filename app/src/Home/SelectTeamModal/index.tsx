import React, { useLayoutEffect, useState } from 'react';
import styled from 'styled-components';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Input from 'components/Input';
import ErrorMessage from 'components/ErrorMessage';
import Loading from 'components/Loading';
import { Member } from 'Home/TeamMember';

const Container = styled(Modal)`
  width: 250px;
`;

const SelectTeam = styled.span`
  margin-bottom: 15px;
  font-size: 16px;
  font-weight: 600;
  color: #fff;
`;

const SelectTeamLink = styled.span`
  margin-bottom: 8px;
  font-size: 16px;
  font-weight: 500;
  color: #5C4FED;
  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

const JoinTeamWrapper = styled.div`
  margin-top: 50px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const JoinTeamTitle = styled.span`
  margin-bottom: 10px;
  font-size: 16px;
  font-weight: 600;
  color: #fff;
`;

const JoinTeamInput = styled(Input)`
  margin: 10px 0;
  width: 200px;
  background: #2F3239;
  font-size: 14px;
`;

const JoinTeamButton = styled(Button)`
  margin-top: 10px;
`;


interface SelectTeamModalProps {
  onCloseModalRequest: () => void;
  onSelectTeam: (teamID: string) => void;
  teamIDs: string[];
  onJoinTeam: (teamID: string) => Promise<void>;
  error: string;
  isLoading: boolean;
}

function SelectTeamModal(props: SelectTeamModalProps) {
  const [joinTeamURL, setJoinTeamURL] = useState('');
  const [joinTeamID, setJoinTeamID] = useState('');


  function getJoinTeamID(): string {
    return joinTeamID;
    /*
    // TODO:
    // https://join.sidekick.app/team-id-is-here
    const splitted = joinTeamURL.split('/');
    if (splitted.length > 0) {
      return splitted[splitted.length - 1];
    }
    return '';
    */
  }

  async function handleJoinTeam() {
    const teamID = getJoinTeamID();
    if (teamID && teamID !== '') {
      await props.onJoinTeam(teamID);
      props.onSelectTeam(teamID);
    }
  }

  return (
    <Container
      onCloseModalRequest={props.onCloseModalRequest}
    >
      <SelectTeam>Choose active team</SelectTeam>
      {props.teamIDs.map(id => (
        <SelectTeamLink
          key={id}
          onClick={() => props.onSelectTeam(id)}
        >
          {id}
        </SelectTeamLink>
      ))}

      <JoinTeamWrapper>
        <JoinTeamTitle>Join new team</JoinTeamTitle>
        <JoinTeamInput
          /*value={joinTeamURL}*/
          value={joinTeamID}
          /*placeholder="Pass the join team URL here"*/
          placeholder="Name of the team to join"
          /*onChange={e => setJoinTeamURL(e.target.value)}*/
          onChange={e => setJoinTeamID(e.target.value)}
        />

        {props.error && (
          <ErrorMessage>
            {props.error}
          </ErrorMessage>
        )}

        {props.isLoading && (
          <Loading text="" />
        )}

        {!props.isLoading && (
          <JoinTeamButton
            onClick={handleJoinTeam}
          >
            Join team
          </JoinTeamButton>
        )}
      </JoinTeamWrapper>
    </Container>
  );
}

export default SelectTeamModal;
