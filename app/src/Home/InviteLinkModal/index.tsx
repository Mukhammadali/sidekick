import React from 'react';
import styled from 'styled-components';
import copy from 'copy-to-clipboard';

import Modal from 'components/Modal';

const Container = styled(Modal)`
  width: 250px;
`;

const Title = styled.span`
  margin-bottom: 15px;
  font-size: 15px;
  font-weight: 500;
  color: #fff;
  text-align: center;
`;

const LinkWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 100%;
  align-content: center;
  align-items: center;
 `;

const Link = styled.div`
  /* border: 1px solid gold; */
  max-width: 100%;

  border-radius: 3px;
  overflow: hidden;
  overflow-x: scroll;
  white-space: nowrap;

  font-size: 14px;
  font-weight: 400;

  padding: 10px 10px;

  background-color: #24272E;
  color: white;

  ::-webkit-scrollbar {
    display:none;
  }
  `;

const CopyButton = styled.div`
  margin-top: 10px;
  font-size: 16px;
  font-weight: 400;
  padding: 5px 0px 5px 8px;
  color: #BFBFBF;
  :hover {
    cursor: pointer;
    color: #fff;
  }
`;


interface InviteLinkModalProps {
  username: string;
  teamName: string;
  email: string;
  onCloseModalRequest: () => void;
}

function InviteLinkModal(props: InviteLinkModalProps) {

  const link = `https://terminal-sidekick.web.app/signup?teamName=${props.teamName}&senderEmail=${props.email}&senderUsername=${props.username}`;

  return (
    <Container onCloseModalRequest={props.onCloseModalRequest}>
      <Title>Invite link</Title>
      <LinkWrapper>
        <Link>
          {link}
        </Link>
        <CopyButton onClick={() => copy(link, { message: 'copied' })}>
          Copy
        </CopyButton>
      </LinkWrapper>
    </Container>
  );
}

export default InviteLinkModal;

