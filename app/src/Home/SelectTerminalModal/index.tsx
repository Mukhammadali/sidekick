import React, { useEffect } from 'react';
import styled from 'styled-components';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Input from 'components/Input';
import { Member } from 'Home/TeamMember';
import { turnTerminalHighlightOn } from 'mainProcess';

const Container = styled(Modal)`
  width: 250px;
`;

const SelectTerminal = styled.span`
  margin-bottom: 15px;
  font-size: 15px;
  font-weight: 500;
  color: #fff;
  text-align: center;
`;

const Info = styled.span`
  font-size: 14px;
  font-weight: 500;
  color: #A0A1A4;
  text-align: center;
`;

const SelectTerminalLink = styled.span`
  max-width: 220px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin-bottom: 8px;
  font-size: 14px;
  font-weight: 500;
  color: #4FA9ED;
  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

interface SelectTerminalModalProps {
  onCloseModalRequest: () => void;
  shellSessions: any[];
  onSelectTerminal: (terminalID: string) => void;
}

function SelectTerminalModal(props: SelectTerminalModalProps) {

  useEffect(() => {
    props.shellSessions.forEach(s => turnTerminalHighlightOn(s.terminalId, `${s.terminalIndex}`));
  }, [props.shellSessions]);

  return (
    <Container
      onCloseModalRequest={props.onCloseModalRequest}
    >
      <SelectTerminal>Select in what terminal to start the session</SelectTerminal>
      {props.shellSessions.length === 0 && (
        <Info>No terminal found. Please open new terminal window.</Info>
      )}

      {props.shellSessions.map(t => (
        <SelectTerminalLink
          key={t.terminalId}
          onClick={() => props.onSelectTerminal(t.terminalId)}
        >Terminal {t.terminalIndex}
        </SelectTerminalLink>
      ))}

      {props.shellSessions.length > 0 && (
        <Info>To start session in a different terminal, open new terminal window.</Info>
      )}
    </Container>
  );
}

export default SelectTerminalModal;
