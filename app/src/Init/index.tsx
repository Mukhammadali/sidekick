import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import Loading from 'components/Loading';
import Button from 'components/Button';
import ErrorMessage from 'components/ErrorMessage';

import { doSTCLSetUp } from '../mainProcess';

const Container = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Section = styled.div`
  margin: 50px 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled.span`
  margin-bottom: 30px;
  width: 100%;
  font-size: 25px;
  font-weight: 500;
  color: #fff;
  text-align: center;
`;

const Text = styled.span`
  margin-bottom: 15px;
  font-size: 15px;
  font-weight: 500;
  color: #fff;
`;

interface InitProps {
  setIsSetUp: (value: boolean) => void;
  // onContinueClick: () => void;
}

function Init(props: InitProps) {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    async function setUp() {
      setIsLoading(true);

      const succ = await doSTCLSetUp();
      if (!succ) {
        setError('Failed to set up the shell');
      } else {
        props.setIsSetUp(true);
      }
      setIsLoading(false);
    }

    setUp();
  }, []);

  function handleButtonClick() {
    if (!isLoading && !error) {
      // props.onContinueClick();
      history.replace('/');
    }
  }

  return (
    <Container>
      <Section>
        <Title>
          Sidekick
        </Title>




        {isLoading && (
          <Text>
            Setting up your shell environment
          </Text>
        )}

        {isLoading && (
          <Loading text="" />
        )}

        {!isLoading && !error && (
          <Text>
            All set up
          </Text>
        )}

        {error && (
          <ErrorMessage>
            {error}
          </ErrorMessage>
        )}
      </Section>

      {!isLoading && !error && <Button
        onClick={handleButtonClick}
      >
        CONTINUE
      </Button>}
    </Container>
  );
}

export default Init;

