import { TerminalEntry } from '@foundryapp/sidekick-terminal-server';
const electron = window.require('electron') as typeof import('electron');

// Is the STCL (Shell Terminal Controller) program installed?
// export function isSTCLInstalled(): Promise<boolean> {
//  return electron.ipcRenderer.invoke('is-stcl-installed');
// }

// Is an STCL command in the default shell's profile file?
export function isSTCLSetUp(): Promise<boolean> {
  return electron.ipcRenderer.invoke('is-stcl-set-up');
}

export function doSTCLSetUp(): Promise<boolean> {
  return electron.ipcRenderer.invoke('do-stcl-set-up');
}

export function isProcessTrusted(): Promise<boolean> {
  return electron.ipcRenderer.invoke('is-process-trusted');
}

export function notifyViewReady() {
  electron.ipcRenderer.send('view-ready');
}

export function notifyUserAuthenticated(userID: string, teamID: string) {
  console.log('notify user authenticated [renderer]', userID, teamID);
  electron.ipcRenderer.send('user-authenticated', { userID, teamID });
};

export function requestShellSessions(): Promise<TerminalEntry[]> {
  return electron.ipcRenderer.invoke('request-shell-sessions');
}

export function changeTeam(teamName: string): Promise<void> {
  return electron.ipcRenderer.invoke('change-team', { teamName });
}

export function disconnect(): Promise<void> {
  return electron.ipcRenderer.invoke('disconnect');
}

export function shareShellSession(userID: string, shellSession: TerminalEntry): Promise<any> {
  return electron.ipcRenderer.invoke('share-shell-session', { userID, shellSession });
}

export function requestPendingSessionInvites(): Promise<any> {
  return electron.ipcRenderer.invoke('request-pending-session-invite');
}

export function rejectShellSession(sessionID: string) {
  electron.ipcRenderer.send('reject-shell-session', { sessionID });
}

export function acceptShellSession(sessionID: string, hostShellSession: any) {
  electron.ipcRenderer.send('accept-shell-session', { sessionID, hostShellSession });
}

export function turnTerminalHighlightOn(terminalID: string, message: string, color = '#ff0000') {
  electron.ipcRenderer.send('turn-terminal-highlight-on', { terminalID, message, color });
}

export function turnTerminalHighlightOff(terminalID: string) {
  electron.ipcRenderer.send('turn-terminal-highlight-off', { terminalID });
}

// So we see logs from the main process in the Chrome debug tools
electron.ipcRenderer.on('console', (event, args) => {
  const [type, ...consoleArgs] = args;
  console[type as 'log' | 'error']?.('[main]:', ...consoleArgs);
});

export default electron;
