import electron from 'api/electron';

export default function isDev(): Promise<boolean> {
  return electron.ipcRenderer.invoke('is-dev');
}
