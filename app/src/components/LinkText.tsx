import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const StyledLink = styled(Link)`
  color: #4FA9ED;
  font-size: 14px;
  font-weight: 400;
  text-decoration: none;
`;

interface LinkTextProps {
  to: string;
  children?: React.ReactNode[] | React.ReactNode;
}

function LinkText(props: LinkTextProps) {
  return (
    <StyledLink
      to={props.to}
    >
      {props.children}
    </StyledLink>
  );
}

export default LinkText;

