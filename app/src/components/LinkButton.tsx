import styled from 'styled-components';
import {Link,} from 'react-router-dom';


const LinkButton = styled(Link)`
  font-size: 18px;
  font-weight: 400;
  text-decoration: none;
  color: #ffffff;

  :hover {
    color: #4967f1;
  }
`;


export default LinkButton;
