import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
  padding: 6px 10px;
  background-color: #42444A;
  color: white;
  font-weight: 400;
  font-size: 14px;
  outline: none;
  border: 1px solid transparent;
  border-radius: 3px;

  ::placeholder {
    color: #A0A1A4;
  }

  :focus {
    border: 1px solid #5C4FED;
  }
`;

interface InputProps {
  type?: string;
  placeholder?: string;
  value: string;
  onChange: (e: any) => void;
}

function Input(props: InputProps) {
  return <StyledInput {...props}/>
}

export default Input;
