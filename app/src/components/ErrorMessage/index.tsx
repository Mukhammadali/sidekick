import styled from 'styled-components';

const ErrorMessage = styled.div`
  color: #FF4C45;
  font-size: 14px;
  font-weight: 600;
`;

export default ErrorMessage;

