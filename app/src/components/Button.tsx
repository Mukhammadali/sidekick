import React, { FunctionComponent } from 'react';
import styled from 'styled-components';


const Content = styled.div`
  padding: 10px 30px;
  display: flex;
  align-items: center;
  justify-content: center;

  color: #FFF;
  font-weight: 600;
  font-size: 14px;
  user-select: none;

  background: #5C4FED;
  border-radius: 3px;
  // border: 1px solid #343434;

  :hover {
    color: #FFFFFF;
    cursor: pointer;
  }
`;

interface ButtonProps {
  className?: string;
  onClick: any;
}

const Button: FunctionComponent<ButtonProps> = ({
 className, children, onClick
}) => {
  return (
    <Content className={className} onClick={onClick} >
      {children}
    </Content>
  );
}


export default Button;
