import styled from 'styled-components';

const Title = styled.h1`
  margin: 0;
  padding: 0;
  font-size: 18px;
  font-weight: 600;
  color: #fff;
`;

export default Title;
