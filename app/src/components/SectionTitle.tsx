import React from 'react';
import styled from 'styled-components';

const SectionTitle = styled.h1`
  text-transform: capitalize;
  font-size: 12px;
  font-weight: 600;
  color: #6E7075;
`;

export default SectionTitle;

