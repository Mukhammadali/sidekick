import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as cors from 'cors';
// import algoliasearch from 'algoliasearch';
import * as sgMail from '@sendgrid/mail';

const corsHandler = cors({ origin: true });

// const algoliaClient = algoliasearch(functions.config().algolia.appid, functions.config().algolia.apikey);

sgMail.setApiKey(functions.config().sendgrid.apikey);

admin.initializeApp();
const db = admin.firestore();

// Source: https://stackoverflow.com/a/52850529/2758318
const isValidDocID = (id: string) => id && /^(?!\.\.?$)(?!.*__.*__)([^/]{1,1500})$/.test(id);
const hasWhiteSpace = (s: string) => s && /\s/g.test(s);


export const createUserWithTeamInvite = functions.runWith({ memory: '1GB' }).https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const { username, teamName, userID, warmUp }: { username: string, teamName: string, userID: string, warmUp: boolean } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!username || !teamName || !userID) {
      response.status(400).send('Cannot create user & team. Either username or team name or user ID is missing in the request.');
      return;
    }

    if (hasWhiteSpace(username) || !isValidDocID(username)) {
      response.status(400).send('Invalid username format. Cannot contain any spaces or special characters.');
      return;
    }
    if (hasWhiteSpace(teamName) || !isValidDocID(teamName)) {
      response.status(400).send('Invalid team name string. Cannot contain any spaces or special characters.');
      return;
    }

    const batch = db.batch();

    const teamDocRef = db.collection('teams').doc(teamName);
    batch.update(teamDocRef, { members: admin.firestore.FieldValue.arrayUnion(userID) });

    const userDocRef = db.collection('users').doc(username);
    batch.set(userDocRef, { username, userID });

    try {
      await batch.commit();
      response.status(200).send({ success: true });
    } catch (error) {
      response.status(500).send(error.message);
    }
  });
});


export const createUserWithTeamInDB = functions.runWith({ memory: '1GB' }).https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const { username, teamName, userID, warmUp }: { username: string, teamName: string, userID: string, warmUp: boolean } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!username || !teamName || !userID) {
      response.status(400).send('Cannot create user & team. Either username or team name or user ID is missing in the request.');
      return;
    }

    if (hasWhiteSpace(username) || !isValidDocID(username)) {
      response.status(400).send('Invalid username format. Cannot contain any spaces or special characters.');
      return;
    }
    if (hasWhiteSpace(teamName) || !isValidDocID(teamName)) {
      response.status(400).send('Invalid team name string. Cannot contain any spaces or special characters.');
      return;
    }

    const batch = db.batch();

    const userDocRef = db.collection('users').doc(username);
    batch.set(userDocRef, { username, userID });

    const teamDocRef = db.collection('teams').doc(teamName);
    batch.set(teamDocRef, { teamID: teamName, members: [userID] });

    try {
      await batch.commit();
      response.status(200).send({ success: true });
    } catch (error) {
      response.status(500).send(error.message);
    }
  });
});

export const isUsernameAvailable = functions.runWith({ memory: '1GB' }).https.onRequest(async (request, response) => {
  corsHandler(request, response, async () => {
    const { username, warmUp }: { username: string, warmUp: boolean } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!username) {
      response.status(400).send('No username provided');
      return;
    }

    if (hasWhiteSpace(username) || !isValidDocID(username)) {
      response.status(400).send('Invalid username format. Cannot contain any spaces or special characters.');
      return;
    }

    try {
      const doc = await db.collection('users').doc(username).get();
      response.status(200).send({
        isAvailable: !doc.exists,
      });
    } catch (error) {
      response.status(500).send(error.message);
    }
  });
});

export const isTeamNameAvailable = functions.runWith({ memory: '1GB' }).https.onRequest(async (request, response) => {
  corsHandler(request, response, async () => {
    const { teamName, warmUp }: { teamName: string, warmUp: boolean } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!teamName) {
      response.status(400).send('No team name provided');
      return;
    }

    // Document Ids should be non-empty strings
    if (hasWhiteSpace(teamName) || !isValidDocID(teamName)) {
      response.status(400).send('Invalid team name string. Cannot contain any spaces or special characters.');
      return;
    }

    try {
      const doc = await db.collection('teams').doc(teamName).get();
      response.status(200).send({
        isAvailable: !doc.exists,
      });
    } catch (error) {
      response.status(500).send(error.message);
    }
  });
});

export const sendTeamInvite = functions.runWith({ memory: '1GB' }).https.onRequest(async (request, response) => {
  corsHandler(request, response, async () => {
    const { teamName, email, senderUsername, senderEmail, warmUp }: { teamName: string, email: string, senderUsername: string, senderEmail: string, warmUp: boolean } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!teamName) {
      response.status(400).send('No team name provided');
      return;
    }
    if (!email) {
      response.status(400).send('No email provided');
      return;
    }
    if (!senderUsername) {
      response.status(400).send('No username provided');
      return;
    }
    if (!senderEmail) {
      response.status(400).send('No username provided');
      return;
    }

    // Document Ids should be non-empty strings
    if (hasWhiteSpace(teamName) || !isValidDocID(teamName)) {
      response.status(400).send('Invalid team name string. Cannot contain any spaces or special characters.');
      return;
    }

    try {
      const team = await db.collection('teams').doc(teamName).get();
      if (!team.exists) {
        response.status(500).send('Team does not exist');
      }

      try {
        await admin.auth().getUserByEmail(senderEmail);
        const inviteLink = `https://terminal-sidekick.web.app/signup?teamName=${teamName}&senderEmail=${senderEmail}&senderUsername=${senderUsername}`;

        await sgMail.send({
          to: email,
          from: senderEmail,
          subject: 'Sidekick Team Invite',
          text: `Team invite link\n\n${inviteLink}`,
        });
      } catch (error) {
        throw new Error('Sender does not exist');
      }
    } catch (error) {
      response.status(500).send(error.message);
    }
  })
});

export const joinTeam = functions.runWith({ memory: '1GB' }).https.onRequest(async (request, response) => {
  corsHandler(request, response, async () => {
    const { teamID, userID, warmUp }: { teamID: string, userID: string, warmUp: string } = request.body;

    if (warmUp) {
      response.status(204).send('Warm up');
      return;
    }

    if (!teamID) {
      response.status(400).send('No team ID provided.');
      return;
    }

    if (!userID) {
      response.status(400).send('No user ID provided.');
      return;
    }

    try {
      // TODO: Do this through transaction
      const ref = db.collection('teams').doc(teamID);
      const teamDoc = await ref.get();
      if (!teamDoc.exists) {
        response.status(404).send(`Team with ID '${teamID}' doesn't exist.`);
        return;
      }

      const { members }: { members: string[] } = teamDoc.data() as any;
      if (!members.includes(userID)) {
        members.push(userID);
        await ref.update({ members });
        response.status(200).send({
          success: true,
        });
      } else {
        response.status(400).send('User is already a member of this team.');
      }
    } catch (error) {
      response.status(500).send(error.message);
    }
  });
});

// export const indexHistory = functions.runWith({ memory: '1GB' }).database.ref('terminal-sidekick/history/{userID}/{commandID}').onCreate((snap, context) => {
//   const { userID, commandID } = context.params;

//   const index = algoliaClient.initIndex(userID);
//   index.saveObjects([])
// });

